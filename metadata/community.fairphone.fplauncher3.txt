Categories:Theming
License:Apache-2.0
Web Site:https://github.com/WeAreFairphone/android_packages_apps_FairphoneLauncher3
Source Code:https://github.com/WeAreFairphone/android_packages_apps_FairphoneLauncher3
Issue Tracker:https://github.com/WeAreFairphone/android_packages_apps_FairphoneLauncher3/issues

Auto Name:Fairphone Launcher 3
Summary:Fairphone Lancher 3 originally developed for Fairphone 2
Description:
Fairphone Launcher originally developed for Fairphone 2.

* Edge Swipe makes it possible to quickly access frequently used apps with just a swipe of the finger from the left or right side of the screen towards the center, and then release it on the desired app. With this feature you can keep your home screen clean and tidy, and only access your important apps when you need to see them.
* The App Life Cycle is a section in the Fairphone where all downloaded and installed apps are vertically displayed. App Life Cycle creates more awareness for the user on how they use their Fairphone in combination with apps. It makes apps become “living things.”

Fairphone stopped developing it's Fairphone specific apps since the release of
Android Marshmallow for the Fairphone 2. Fairphone wants the community to take
care and maintain those apps.
.

Repo Type:git
Repo:https://github.com/WeAreFairphone/android_packages_apps_FairphoneLauncher3.git

Build:2.0,8
    commit=26c46d63eb9bbc25e836c535c5688cb885cb0be2
    gradle=yes
    prebuild=echo 'android { lintOptions { checkReleaseBuilds false } }' >> build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.0
Current Version Code:8
